import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;


public class Main {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		
		
		HashMap<String,Integer> blackList = new HashMap<String,Integer>();
		
		   DatagramSocket serverSocket = new DatagramSocket(9876);
           byte[] receiveData = new byte[1024];
           byte[] sendData = new byte[1024];
           
           
           while(true)
              {
                 DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                 serverSocket.receive(receivePacket);
                 String sentence = new String( receivePacket.getData());
                 System.out.println("RECEIVED: " + sentence);
                 
                 if(blackList.containsKey(sentence)){
                	 if(blackList.get(sentence)>=2){
                         InetAddress IPAddress = receivePacket.getAddress();
                         int port = receivePacket.getPort();
                         String warning = sentence;
                         sendData = warning.getBytes();
                         DatagramPacket sendPacket =
                         new DatagramPacket(sendData, sendData.length, IPAddress, port);
                         serverSocket.send(sendPacket);
                	 }
                	 else
                	 blackList.put(sentence, blackList.get(sentence)+1);
                 }
                 else
                	 blackList.put(sentence,1);
              }

	}

}
